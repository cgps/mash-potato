const fs = require('fs');
const path = require('path');
const { getClosestMatch } = require('mash-node-native');

const referencesDir = path.join(__dirname, '..', '..', 'references');
const referenceMshFile = path.join(referencesDir, 'refseq-all-k16-s400.msh');
const metadataFile = path.join(referencesDir, 'refseq-all-k16-s400.csv');

function parseCSV(csv, delimiter = ',', newline = '\n') {
  return csv.split(newline).map(line => {
    if (line === '') {
      return null;
    }
    return line.split(delimiter);
  });
}

function parseRefseqMetadata() {
  return parseCSV(fs.readFileSync(metadataFile, 'utf8'));
}

const refseqMetadata = parseRefseqMetadata();

function queryFile(name, filepath) {
  const match = getClosestMatch(referenceMshFile, filepath);
  if (match === null) {
    return null;
  }
  const info = refseqMetadata[match.referenceIndex - 1];
  if (info === null) {
    return null;
  }
  return [
    name,
    info[0],
    info[1],
    info[2],
    info[3],
    match.mashDistance,
    match.pValue,
    `${match.matchingHashes}/${match.allHashes}`,
  ];
}

function queryFolder(folder) {
  const files = fs.readdirSync(folder);
  for (const file of files) {
    if (file[0] !== '.') {
      const results = queryFile(file, path.join(folder, file));
      if (results !== null) {
        console.log(results.join(','));
      } else {
        console.log([ file, 'ERROR: NO MATCH'].join(','));
      }
    }
  }
}

function main() {
  console.log([ 'File', 'referenceId', 'taxId', 'speciesTaxId', 'scientificName', 'mashDistance', 'pValue', 'matchingHashes' ].join(','));
  const folder = process.argv[2];
  queryFolder(folder);
}

main();
