const fs = require('fs');
const path = require('path');
const tmp = require('tmp');
const { getTopMatch } = require('mash-node-wrapper');
const papaparse = require('papaparse');

const referencesDir = path.join(__dirname, '..', '..', 'references');
const referenceMshFile = path.join(referencesDir, 'refseq-all-k16-s400.msh');
const metadataFile = path.join(referencesDir, 'assembly_summary_refseq.txt');

function parseCSV(csv, delimiter = '\t', newline = '\r') {
  return papaparse.parse(csv, {
    delimiter,
    newline,
    header: false,
    dynamicTyping: false,
    skipEmptyLines: true,
  });
}

function parseRefseqMetadata() {
  const results = parseCSV(fs.readFileSync(metadataFile, 'utf8'));
  return results.data;
}

const refseqMetadata = parseRefseqMetadata();

function getRefseqMetadata([ id ]) {
  for (let i = 0; i < refseqMetadata.length; i++) {
    if (refseqMetadata[i][0] === id) {
      return refseqMetadata[i];
    }
  }
  return null;
}

function queryFile({ name, contents }) {
  const tmpobj = tmp.fileSync({ postfix: name });
  fs.writeSync(tmpobj.fd, contents);
  const topMatch = getTopMatch(referenceMshFile, tmpobj.name);
  tmpobj.removeCallback();
  if (topMatch === null) {
    return null;
  }
  const info = getRefseqMetadata(topMatch);
  return {
    referenceId: info[0],
    taxId: info[1],
    speciesTaxId: info[2],
    scientificName: info[3],
    mashDistance: topMatch[2],
    pValue: topMatch[3],
    matchingHashes: topMatch[4],
  };
}

function queryFiles(files) {
  return files.map(file => queryFile(file));
}

module.exports = {
  queryFile,
  queryFiles,
};
