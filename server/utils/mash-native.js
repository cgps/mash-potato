const fs = require('fs');
const path = require('path');
const tmp = require('tmp');
const now = require('performance-now');

const { closestMatch } = require('mash-node-native');

const referencesDir = path.join(__dirname, '..', '..', 'references');
const referenceMshFile = path.join(referencesDir, 'refseq-all-k16-s400.msh');
const metadataFile = path.join(referencesDir, 'refseq-all-k16-s400.csv');

function parseCSV(csv, delimiter = ',', newline = '\n') {
  return csv.split(newline).map(line => {
    if (line === '') {
      return null;
    }
    return line.split(delimiter);
  });
}

function parseRefseqMetadata() {
  return parseCSV(fs.readFileSync(metadataFile, 'utf8'));
}

const refseqMetadata = parseRefseqMetadata();

function createTmpFile(postfix, data) {
  return new Promise((resolve, reject) => {
    tmp.file({ postfix }, (err, filePath, fd, removeCallback) => {
      if (err) return reject(err);
      return fs.write(fd, data, writeErr => {
        if (writeErr) return reject(writeErr);
        return resolve({ filePath, removeCallback });
      });
    });
  });
}

function queryFile({ name, contents }) {
  return createTmpFile(name, contents)
    .then(({ filePath, removeCallback }) =>
      closestMatch(referenceMshFile, filePath).then(match => {
        removeCallback();
        return match;
      })
    ).then(match => {
      if (match === null) return null;
      const info = refseqMetadata[match.referenceIndex];
      if (info === null) return null;
      return {
        name,
        referenceId: info[0],
        taxId: info[1],
        speciesTaxId: info[2],
        scientificName: info[3],
        mashDistance: match.mashDistance,
        pValue: match.pValue,
        matchingHashes: `${match.matchingHashes}/${match.allHashes}`,
      };
    });
}

module.exports = {
  queryFile,
};
