/* eslint no-param-reassign: ["error", { "props": false }] */

const path = require('path');
const JSZip = require('jszip');
const createMashSpecieator = require('mash-specieator');

const referencesDir = require('mash-sketches');
const sketchFilePath = path.join(referencesDir, 'refseq-archaea-bacteria-fungi-viral-k16-s400.msh');
const metadataFilePath = path.join(referencesDir, 'refseq-archaea-bacteria-fungi-viral-k16-s400.csv');
const specieator = createMashSpecieator(sketchFilePath, metadataFilePath);

function getRequestFastaFile(req) {
  if (!req.body) throw new Error('Request body is empty.');
  const contentType
    = (req.headers['content-type'] || 'text/plain').toLowerCase();
  const compressed = req.query.compressed || 'false';
  return new Promise(resolve => {
    if (contentType.includes('text/plain')) {
      if (compressed === 'true') {
        return JSZip
          .loadAsync(req.body, { base64: true, compression: 'DEFLATE' })
          .then(zip => zip.file('query.fasta').async('string'))
          .then(uncompressed => resolve(uncompressed));
      }
      return resolve(req.body);
    }
    throw new Error('Invalid request content type.');
  });
}

function queryFastaFile(fastaFileContent) {
  return specieator.queryContents(fastaFileContent).then(match =>
    match || ({ referenceId: 'No match' })
  );
}

function fasta(req, res, next) {
  try {
    getRequestFastaFile(req)
      .then(queryFastaFile)
      .then(results => {
        req.body = null;
        return res.status(200).send(results);
      })
      .catch(err => next(err));
  } catch (error) {
    next(error);
  }
}

module.exports = {
  fasta,
};
