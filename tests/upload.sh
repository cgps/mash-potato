files="/Users/kad/Documents/FASTA files/Staphylococcus aureus/SA_003/"
for file in "$files"/*.fa
do
  curl -i -H "Content-type: text/plain; charset=UTF-8" \
     -X POST  http://localhost:8080/api/1.0/query/ \
     --data-binary @"$file" &
done
