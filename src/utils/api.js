import configUtils from '^/utils/config';
import Worker from 'worker?name=file.worker.js!./worker';

const REQUEST_TYPES = {
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
};

function ajax(type, path, data, callback) {
  const apiUrl = configUtils.get('apiUrl');
  window.jQuery.ajax({
    type,
    data: JSON.stringify(data, null, 4),
    dataType: 'json',
    url: `${apiUrl}${path}`,
    contentType: 'text/plain; charset=UTF-8',
  })
  .done(res => callback(null, res))
  .fail(error => callback(error, null));
}

function query(files, callback) {
  return ajax(REQUEST_TYPES.POST, '/fasta?compressed=true', files, callback);
}

function uploadFile(fileHandler, callback) {
  const worker = new Worker;

  worker.onmessage = function (event) {
    const contents = event.data;
    if (typeof contents === 'string') {
      query(contents, (error, result) => {
        if (error) {
          callback(error, null);
        } else {
          callback(null, result);
        }
      });
    } else {
      callback(contents.error, null);
    }
  };

  worker.postMessage(fileHandler);
}

module.exports = {
  uploadFile,
};
