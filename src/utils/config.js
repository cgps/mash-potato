const config = {
  apiUrl: '/api/1.0',
};

function get(key) {
  if (key) {
    return config[key];
  }
  return config;
}

function set(newValues) {
  Object.assign(config, newValues);
}

module.exports = {
  get,
  set,
};
