const JSZip = require('jszip');

function readFile(file, callback) {
  if (typeof FileReader === 'undefined') {
    try {
      const reader = new FileReaderSync();
      callback(null, reader.readAsText(file));
    } catch (e) {
      callback(`[WebWorker] Failed to read file: ${file.name}. ${e}`, null);
    }
  } else {
    const fileReader = new FileReader();
    fileReader.onload = function handleLoad(event) {
      callback(null, event.target.result);
    };
    fileReader.onerror = function handleError(e) {
      callback(`[WebWorker] Failed to read file: ${file.name}. ${e}`, null);
      callback(file.name, null);
    };
    fileReader.readAsText(file);
  }
}

onmessage = function (event) {
  console.log('[Worker]', 'Processing file', event.data.name);
  readFile(event.data, (error, contents) => {
    if (error) {
      postMessage({ error });
    }
    const zip = new JSZip();
    zip.file('query.fasta', contents);
    zip.generateAsync({ type: 'base64', compression: 'DEFLATE' }, data => {
      console.log('[Worker]', 'Compressing...', data);
    })
    .then(compressed => {
      console.log('[Worker]', 'Compressed file', compressed.length, contents.length, compressed.length * 100 / contents.length);
      postMessage(compressed);
    });
  });
};
