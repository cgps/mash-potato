import React from 'react';
import Panel from 'muicss/lib/react/panel';
import Button from 'muicss/lib/react/button';

export default function ({ results, onResetFiles }) {
  return (
    <div className="mp-info-list">
      <div className="mui--text-display1">
        Results
        { !onResetFiles ? null :
          <span className="mui--pull-right">
            <Button size="large" color="primary" variant="raised" onClick={onResetFiles}>Try Another</Button>
          </span>
        }
      </div>
      <Panel>
        <table className="mui-table mui-table--bordered">
          <thead>
            <tr>
              <td>File</td>
              <td>Organism scientific name</td>
              <td>Taxonomy ID <br />(Species taxonomy ID)</td>
              <td>RefSeq assembly</td>
              <td>Mash distance</td>
              <td>p-value</td>
              <td>Matching hashes</td>
            </tr>
          </thead>
          <tbody>
            {
              results.map(({ name, status, referenceId, taxId, speciesTaxId, scientificName, pValue, mashDistance, matchingHashes }, index) => (
                <tr key={index}>
                  <td>{ name }</td>
                  <td>{ status }{ scientificName }</td>
                  <td>
                    <a href={`http://www.ebi.ac.uk/ena/data/view/Taxon:${taxId}`} target="_blank">{ taxId }</a>
                    {
                      taxId === speciesTaxId ? null : [
                        <br />,
                        <a href={`http://www.ebi.ac.uk/ena/data/view/Taxon:${speciesTaxId}`} target="_blank">{ speciesTaxId }</a>,
                      ]
                    }
                  </td>
                  <td>
                    <a href={`http://www.ncbi.nlm.nih.gov/assembly/${referenceId}/`} target="_blank">{ referenceId }</a>
                  </td>
                  <td>{ mashDistance && parseFloat(mashDistance).toPrecision(8) }</td>
                  <td>{ pValue && parseFloat(pValue).toPrecision(8) }</td>
                  <td>{ matchingHashes }</td>
                </tr>
              ))
            }
          </tbody>
        </table>
      </Panel>
    </div>
  );
}
