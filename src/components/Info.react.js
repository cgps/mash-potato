import '^/css/info.css';

import React from 'react';

export default React.createClass({

  displayName: 'Info',

  onClick(event) {
    event.preventDefault();
    $('input[type="file"]').click();
  },

  render() {
    return (
      <div className="mui--text-center mp-info">
        <div className="mui--text-display3">Mash Potato</div>
        <div className="mui--text-display2">
          Genome identification using <a href="http://mash.readthedocs.io/en/latest/index.html" target="_blank">Mash</a>
        </div>
        <div className="mui--text-display1">Drag and drop files or <a href="" onClick={this.onClick}> select files</a></div>
      </div>
    );
  },

});
