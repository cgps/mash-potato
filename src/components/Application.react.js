import React from 'react';

import Appbar from 'muicss/lib/react/appbar';
import Container from 'muicss/lib/react/container';
import Button from 'muicss/lib/react/button';
import Upload from './Upload.react';

export default React.createClass({

  displayName: 'Application',

  render() {
    return (
      <div className="mp-application">
        <Appbar>
          <div className="mui-container mui--appbar-height mui--appbar-line-height">
            <div className="mp-application-title">
              Mash Potato
            </div>
            <div className="mp-application-links">
              <Button>How does it work</Button>
              <Button>API</Button>
              <a className="mui-btn" href="https://gitlab.com/cgps/mash-potato" target="_blank">Gitlab</a>
            </div>
          </div>
        </Appbar>
        <div className="mp-application-container">
          <Container>
            <div className="mui--appbar-height"></div>
            <div className="mui--appbar-height"></div>
            <Upload />
          </Container>
        </div>
        <footer>
          <div className="mui-container mui--text-center">
            Maintained by <a href="http://www.pathogensurveillance.net/">The Centre for Genomic Pathogen Surveillance (CGPS)</a>
          </div>
        </footer>
      </div>
    );
  },

});
