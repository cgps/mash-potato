import '^/css/cgps-upload.css';

import 'jquery-draghover';

import React from 'react';
import Panel from 'muicss/lib/react/panel';
import Button from 'muicss/lib/react/button';

import DragAndDrop from './DragAndDrop.react';
import Info from './Info.react';
import FileList from './FileList.react';
import ResultList from './ResultList.react';

import ApiUtils from '^/utils/api';

const initialState = {
  error: null,
  isUploading: false,
  results: [],
  files: [],
};

const supportedFileTypes = [ 'fa', 'fasta' ];

function checkFileType(filename) {
  for (const filetype of supportedFileTypes) {
    if (filename.indexOf(filetype, filename.length - filetype.length) !== -1) {
      return true;
    }
  }
  return false;
}

export default React.createClass({

  displayName: 'Upload',

  propTypes: {
    className: React.PropTypes.string,
  },

  getInitialState() {
    return initialState;
  },

  componentDidMount() {
  },

  onFileInputChange(event) {
    event.preventDefault();
    if (event && event.target && event.target.files) {
      this.queueFiles(Array.from(event.target.files));
    }
  },

  resetUpload() {
    this.setState(initialState);
  },

  queueFiles(fileHandlers) {
    const newFiles = [];
    for (const handler of fileHandlers) {
      const name = handler.name;
      if (checkFileType(name.toLowerCase())) {
        newFiles.push({
          name,
          handler,
          status: 'Ready to upload',
        });
      } else {
        newFiles.push({
          name,
          status: 'File type is not supported',
        });
      }
    }
    this.setState({
      files: this.state.files.concat(newFiles),
    });
  },

  uploadFiles() {
    const { files } = this.state;
    if (files.length) {
      this.setState({
        isUploading: true,
        results: [],
      });
      this.uploadFirstFile();
    }
  },

  uploadFirstFile() {
    const { files, results } = this.state;
    const file = files.shift();
    if (file) {
      const { name, handler } = file;
      this.setState({
        results: results.concat({ name, status: 'Uploading...' }),
      });
      ApiUtils.uploadFile(handler, (err, result) => {
        const newResults = [];
        if (err) {
          newResults.push({
            name,
            status: err.responseJSON ? err.responseJSON.err : err.responseText,
          });
        } else {
          newResults.push({ name, ...result });
        }
        this.setState({
          results: results.concat(newResults),
        });
        this.uploadFirstFile();
      });
    } else {
      this.setState({ isUploading: false });
    }
  },

  renderMainContent() {
    const { error, isUploading, results, files } = this.state;

    if (error) {
      return (
        <Panel>
          <p>
            { error }
          </p>
          <Button size="large" color="primary" variant="raised" onClick={this.resetUpload}>Try again</Button>
        </Panel>
      );
    } else if (isUploading || results.length > 0) {
      return (
        <div>
          <ResultList results={results} onResetFiles={isUploading ? null : this.resetUpload} />
          { !isUploading ? null :
            <div className="mp-mashing mui--text-center">
              <img src="/images/mashing.gif" />
            </div>
          }
        </div>
      );
    } else if (files.length > 0) {
      return (
        <FileList files={files} onUploadFiles={this.uploadFiles} />
      );
    }
    return (
      <Info />
    );
  },

  render() {
    return (
      <DragAndDrop onDrop={files => this.queueFiles(files)}>
        { this.renderMainContent() }
        <input
          ref="fileInput"
          type="file"
          multiple="multiple"
          accept={supportedFileTypes.join(',')}
          className="cgps-file-upload"
          onChange={this.onFileInputChange}
        />
      </DragAndDrop>
    );
  },

});
