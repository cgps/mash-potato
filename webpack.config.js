const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HashPlugin = require('hash-webpack-plugin');

const srcFolder = path.join(__dirname, 'src');

const resolve = {
  alias: {
    '^': srcFolder,
  },
  root: path.resolve(__dirname, 'node_modules'),
};

const babelPresets = [ 'react', 'es2015', 'stage-0' ];

const postcss = [
  require('autoprefixer')({ browsers: [ 'last 2 versions' ] }),
  require('postcss-input-style'),
];

const cssLoader = {
  test: /.css$/,
  loaders: [ 'style', 'css', 'postcss' ],
};

const devConfig = {
  devtool: '#eval-source-map',
  entry: [
    'webpack-hot-middleware/client',
    './src/app',
  ],
  output: {
    path: path.join(__dirname, 'public', 'js'),
    filename: 'mash-potato.js',
    publicPath: '/',
    library: 'MashPotato',
    libraryTarget: 'umd',
  },
  resolve,
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        include: [
          path.resolve(__dirname, 'src'),
        ],
        query: {
          presets: babelPresets,
          plugins: [
            [ 'react-transform', {
              'transforms': [ {
                'transform': 'react-transform-hmr',
                'imports': [ 'react' ],
                'locals': [ 'module' ],
              }, {
                'transform': 'react-transform-catch-errors',
                'imports': [ 'react', 'redbox-react' ],
              } ],
            } ],
          ],
        },
      },
      cssLoader,
    ],
  },
  postcss,
};

const prodConfig = {
  entry: './src/app',
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'mash-potato.js',
    publicPath: '/',
    library: 'MashPotato',
    libraryTarget: 'umd',
  },
  resolve,
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false,
      },
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
    }),
    new ExtractTextPlugin('../styles.css'),
    new HashPlugin({ path: './', fileName: 'webpack.hash' }),
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        include: [
          path.resolve(__dirname, 'src'),
        ],
        query: {
          presets: babelPresets,
        },
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style', 'css', 'postcss'),
      },
    ],
  },
  postcss,
};

module.exports = process.env.NODE_ENV === 'production' ? prodConfig : devConfig;
