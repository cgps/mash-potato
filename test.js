const p = new Promise((resolve, reject) => {
  resolve(1);
  throw new Error('Fail');
});

p
  .then(() => 2)
  .then(result => console.log(result))
  .catch(error => console.error(':(', error));
